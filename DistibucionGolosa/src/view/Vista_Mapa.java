package view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import archivos.Escritura;
import logic.DistribucionGolosa;

public class Vista_Mapa {
	public JFrame frame;
	private JMapViewer mapa;
	
	private DistribucionGolosa distribucion;
	private ArrayList<Color> colores;

	public Vista_Mapa(DistribucionGolosa dG) {
		distribucion=dG;
		colores = new ArrayList<Color>();
		colores.add(Color.RED);
		colores.add(Color.BLUE);
		colores.add(Color.MAGENTA);
		colores.add(Color.GREEN);
		initialize();
	}

	public void initialize() {
		frame = new JFrame();
		frame.setSize(800,800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("JMapViewer");
		frame.setLocationRelativeTo(null);
		
		mapa = new JMapViewer();
		mapa.setZoomControlsVisible(false);
		
		// Creacion del menu Opciones
		JMenu menu = new JMenu("Opciones");
		// Items del menu
		JMenuItem menuItem = new JMenuItem("Ejecutar Algoritmo");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					distribucion.resolverDistribucion();
					for (int i = 0; i < distribucion.getCantCentrosDeDistribucionElegidos(); i++) {
						int numRandom = (int) (Math.random()*4);
						for (int j = 0; j < distribucion.getCantClientes(); j++) {
							agregarCamino(j,i,numRandom);
						}
					}
					Escritura.escrituraDeSolucion(distribucion.getCentrosDeDistribucionElegidos());
					JOptionPane.showMessageDialog(frame, "El costo total de abrir estos centros es: " + distribucion.getCostoTotalSolucion());
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(frame, "El numero de centros se pasa de lo permitido");
				}
			}
		});
		
		menu.add(menuItem);
		
		JMenuItem menuItem2 = new JMenuItem("Mostrar Estadisticas");
		menuItem2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Ver_Estadisticas estadisticas = new Ver_Estadisticas(distribucion);
					estadisticas.initialize();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		});
		menu.add(menuItem2);
		
		JMenuItem menuItem3 = new JMenuItem("Volver");
		menuItem3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		menu.add(menuItem3);
		
		// Creacion de la BARRA de menu
		JMenuBar barra = new JMenuBar();
		
		// Agrego los menus a la barra
		barra.add(menu);
		
		// Agrego la barra al menu
		frame.setJMenuBar(barra);
		
		for (int i = 0; i < distribucion.getCantClientes(); i++) {
			Coordinate coordenada = new Coordinate(distribucion.getClientes().get(i).getLatitud(),
					distribucion.getClientes().get(i).getLongitud());
			agregarMarcador(coordenada, Color.ORANGE,distribucion.getClientes().get(i).getNombre());
		}
		
		for (int j = 0; j < distribucion.getCantCentrosDeDistribucion(); j++) {
			Coordinate coordenada = new Coordinate(distribucion.getCentrosDeDistribucion().get(j).getLatitud(),
					distribucion.getCentrosDeDistribucion().get(j).getLongitud());
			agregarMarcador(coordenada, Color.YELLOW,distribucion.getCentrosDeDistribucion().get(j).getNombre());
		}
		
		Coordinate coordenada = new Coordinate(distribucion.getClientes().get(0).getLatitud(),
				distribucion.getClientes().get(0).getLongitud());
		mapa.setDisplayPosition(coordenada, 16);
		
		frame.getContentPane().add(mapa);
	}
	
	private void agregarCamino(int numeroDeCliente, int numeroDeCentro,int numRandomElegido) {
		ArrayList<Coordinate> coordenas = new ArrayList<Coordinate>();
		coordenas.add(new Coordinate(distribucion.getClientes().get(numeroDeCliente).getLatitud(),
				distribucion.getClientes().get(numeroDeCliente).getLongitud()));
		coordenas.add(new Coordinate(distribucion.getClientes().get(numeroDeCliente).getLatitud(),
				distribucion.getClientes().get(numeroDeCliente).getLongitud()));
		coordenas.add(new Coordinate(distribucion.getCentrosDeDistribucionElegidos().get(numeroDeCentro).getLatitud(),
				distribucion.getCentrosDeDistribucionElegidos().get(numeroDeCentro).getLongitud()));
		
		MapPolygon poligono = new MapPolygonImpl(coordenas);
		poligono.getStyle().setColor(colores.get(numRandomElegido));
		mapa.addMapPolygon(poligono);
	}
	
	private void agregarMarcador(Coordinate coord, Color color,String nombre) {
		MapMarker marcador1 = new MapMarkerDot(nombre,coord);
		marcador1.getStyle().setBackColor(color);
		marcador1.getStyle().setColor(Color.ORANGE);
		mapa.addMapMarker(marcador1);
	}
	
	public void dispose() {
		
	}
}
